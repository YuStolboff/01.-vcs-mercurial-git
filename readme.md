1. Create an account on https://bitbucket.org or http://github.com 
 2. Create Git-repository "00_Entrance_test_task" and put there your entrance test task source code
 3. Choose tool that feats you best. SourceTree is highly recommended
 4. Try  
 Commit  
 Push  
 Pull  
 Create a new branch  
 Merge branches  
 Merge revisions while pulling changes  
 Resolve merge conflicts  
 Overview versioned files history  
 * Rebase while pulling and pushing changes  
 * Install Mercurial plugin for Eclipse IDE or other IDE you use  
 * Pull/merge requests  
 In a new repository "01_VCS"  